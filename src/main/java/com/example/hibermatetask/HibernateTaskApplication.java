package com.example.hibermatetask;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(info = @Info(title = "Hibernate task", version = "1.3.2", description = "information about student, skills and teacher"))
@SpringBootApplication
public class HibernateTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(HibernateTaskApplication.class, args);
    }

}
