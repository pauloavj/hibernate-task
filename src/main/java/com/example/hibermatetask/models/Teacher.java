package com.example.hibermatetask.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;

    @OneToMany
    @JoinColumn(name = "teacher_id")
    List<Student> studentList;


    @JsonGetter("studentList")
    public List<String> studentList() {
        if(studentList != null) {
            return studentList.stream()
                    .map(student -> {
                        return "/api/v1/student/" + student.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }
    @ManyToMany
    @JoinTable(
            name = "teacher_skill",
            joinColumns = @JoinColumn(name = "teacher_id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id")
    )
    public List<Skill> skills;

    @JsonGetter("skills")
    public List<String> skills(){
        if (skills != null) {
            return skills.stream()
                    .map(skill ->
                        "/api/v1/skills/" + skill.getId()
                    ).collect(Collectors.toList());
        }
        return null;
    }

    public Teacher() {
    }
}
