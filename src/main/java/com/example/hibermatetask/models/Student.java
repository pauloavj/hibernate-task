package com.example.hibermatetask.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
@Getter
@Setter
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "teacher_id")
    public Teacher teacher;

    @JsonGetter("teacher")
    public String teacher(){
        if(teacher != null){
            return "/api/v1/teacher/" + teacher.getId();
        }else{
            return null;
        }
    }




//    private Skills skills;

    public Student() {

    }
}
