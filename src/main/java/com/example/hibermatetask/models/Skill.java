package com.example.hibermatetask.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
public class Skill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "skill_description")
    private String skillDescription;

    @ManyToMany
    @JoinTable(
            name = "teacher_skill",
            joinColumns = @JoinColumn(name = "skill_id"),
            inverseJoinColumns = @JoinColumn(name = "teacher_id")
    )
    public List<Teacher> teachers;

    @JsonGetter("teachers")
    public List<String> skillsGetter(){
        if (teachers != null){
            return teachers.stream()
                    .map(teacher ->
                        "/api/v1/teachers/" + teacher.getId()
                    ).collect(Collectors.toList());
        }
        return null;
    }

    public Skill() {
    }
}
