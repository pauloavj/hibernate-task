package com.example.hibermatetask.controllers;

import com.example.hibermatetask.models.Student;
import com.example.hibermatetask.repositories.StudentRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/student")
@Tag(name = "Student", description = "describe some information about the student")
public class StudentController {
    private final StudentRepository studentRepository;

    @Autowired
    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Operation(summary = "Get all students.")
    @GetMapping
    public ResponseEntity<List<Student>> getAllStudents(){
        List<Student> students = studentRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(students, status);
    }

    @Operation(summary = "Get specific student by choosing an id.")
    @GetMapping("/{id}")
    public ResponseEntity<Student> getSpecificStudent(@PathVariable Long id){
        HttpStatus status;
        Student getStudent = new Student();
        if (!studentRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(getStudent, status);
        }
        getStudent = studentRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(getStudent, status);
    }

    @Operation(summary = "Add a specific student.")
    @PostMapping
    public ResponseEntity<Student> addStudent(@RequestBody Student student){
        Student studentReturn = studentRepository.save(student);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(studentReturn,status);
    }

    @Operation(summary = "Update a specific student by choosing an id.")
    @PutMapping("/{id}")
    public ResponseEntity<Student> updateStudent(@RequestBody Student student, @PathVariable Long id){
        HttpStatus status;
        Student returnStudent = new Student();
        if(!Objects.equals(id, student.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnStudent, status);
        }
        returnStudent = studentRepository.save(student);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnStudent,status);
    }

    @Operation(summary = "Delete a specific student by id")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteStudent(@PathVariable Long id){
        HttpStatus status;
        if(studentRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            studentRepository.deleteById(id);
            return new ResponseEntity<>("Student removed", status);
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(null, status);
    }
}
