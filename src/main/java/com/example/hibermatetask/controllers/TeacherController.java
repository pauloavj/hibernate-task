package com.example.hibermatetask.controllers;


import com.example.hibermatetask.models.Teacher;
import com.example.hibermatetask.repositories.TeacherRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/teacher")
@Tag(name = "teacher", description = "teacher information")
public class TeacherController {
    private final TeacherRepository teacherRepository;

    @Autowired
    public TeacherController(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    @Operation(summary = "Get all teachers.")
    @GetMapping
    public ResponseEntity<List<Teacher>> getAllTeachers(){
        List<Teacher> teachers = teacherRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(teachers, status);
    }

    @Operation(summary = "Get specific teacher by choosing an id.")
    @GetMapping("/{id}")
    public ResponseEntity<Teacher> getSpecificTeacher(@PathVariable Long id){
        HttpStatus status;
        Teacher getTeacher = new Teacher();
        if (!teacherRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(getTeacher, status);
        }
        getTeacher = teacherRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(getTeacher, status);
    }

    @Operation(summary = "Add a specific teacher.")
    @PostMapping
    public ResponseEntity<Teacher> addTeacher(@RequestBody Teacher teacher){
        Teacher teacherReturn = teacherRepository.save(teacher);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(teacherReturn,status);
    }

    @Operation(summary = "Update a specific teacher by choosing an id.")
    @PutMapping("/{id}")
    public ResponseEntity<Teacher> updateTeacher(@RequestBody Teacher teacher, @PathVariable Long id){
        HttpStatus status;
        Teacher returnTeacher = new Teacher();
        if(!Objects.equals(id, teacher.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnTeacher, status);
        }
        returnTeacher = teacherRepository.save(teacher);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnTeacher,status);
    }

    @Operation(summary = "Delete a specific teacher by id")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTeacher(@PathVariable Long id){
        HttpStatus status;
        if(teacherRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            teacherRepository.deleteById(id);
            return new ResponseEntity<>("Teacher removed", status);
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(null, status);
    }
}
