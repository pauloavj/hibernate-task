package com.example.hibermatetask.controllers;

import com.example.hibermatetask.models.Skill;
import com.example.hibermatetask.models.Skill;
import com.example.hibermatetask.models.Student;
import com.example.hibermatetask.repositories.SkillsRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/skills")
@Tag(name = "Skills", description = "Tells which skills the teacher has.")
public class SkillsController {
    private final SkillsRepository skillsRepository;

    @Autowired
    public SkillsController(SkillsRepository skillsRepository) {
        this.skillsRepository = skillsRepository;
    }

    @Operation(summary = "Get all skills.")
    @GetMapping
    public ResponseEntity<List<Skill>> getAllSkills(){
        List<Skill> skills = skillsRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(skills, status);
    }

    @Operation(summary = "Get specific skill by choosing an id.")
    @GetMapping("/{id}")
    public ResponseEntity<Skill> getSpecificSkill(@PathVariable Long id){
        HttpStatus status;
        Skill getSkill = new Skill();
        if (!skillsRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(getSkill, status);
        }
        getSkill = skillsRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(getSkill, status);
    }

    @Operation(summary = "Add a specific skill.")
    @PostMapping
    public ResponseEntity<Skill> addSkill(@RequestBody Skill skill){
        Skill skillReturn = skillsRepository.save(skill);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(skillReturn, status);
    }

    @Operation(summary = "Update a specific skill by choosing an id.")
    @PutMapping("/{id}")
    public ResponseEntity<Skill> updateSkill(@RequestBody Skill skill, @PathVariable Long id){
        HttpStatus status;
        Skill returnSkill = new Skill();
        if(!Objects.equals(id, skill.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnSkill, status);
        }
        returnSkill = skillsRepository.save(skill);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnSkill,status);
    }

    @Operation(summary = "Delete a specific skill by id")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSkill(@PathVariable Long id){
        HttpStatus status;
        if(skillsRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            skillsRepository.deleteById(id);
            return new ResponseEntity<>("Skill removed", status);
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(null, status);
    }
}
