##Task: Hibernate

Create an application using Hibernate which stores information about teachers, students and skill.

A student is assigned to one and only one teacher, but a teacher may have many students.

You need to record the first and last name of the all the teachers and students.

Teachers may possess certain skill (Java, SQL, JavaScript) but the same skill may be shared by many teachers.

You only need to store  the description each skill.

Create appropriate endpoints to enable full CRUD for all of these entities.

When returning related resources, only return the URI of the resource i.e "/api/v1/skill/1"